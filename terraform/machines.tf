resource "aws_instance" "noeud1" {
  ami                    = var.ami_id
  instance_type          = var.vm_type
  key_name               = aws_key_pair.generated.key_name
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = ["${aws_security_group.master.id}"]

  user_data = <<-EOL
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -yf python3 python3-pip
    sudo adduser ${var.user}
    echo "${var.user}:${var.password}" | sudo chpasswd
    sudo usermod -aG sudo ${var.user}
    sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo systemctl restart ssh
    EOL
  tags = {
    Name = "noeud1"
  }

  depends_on = [
    aws_vpc.vpc,
    aws_subnet.subnet,
  ]
}

resource "aws_instance" "noeud2" {
  ami                    = var.ami_id
  instance_type          = var.vm_type
  key_name               = aws_key_pair.generated.key_name
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = ["${aws_security_group.master.id}"]

  user_data = <<-EOL
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -yf python3 python3-pip
    sudo adduser ${var.user}
    echo "${var.user}:${var.password}" | sudo chpasswd
    sudo usermod -aG sudo ${var.user}
    sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo systemctl restart ssh
    EOL
  tags = {
    Name = "noeud2"
  }

  depends_on = [
    aws_vpc.vpc,
    aws_subnet.subnet,
  ]
}

resource "aws_instance" "noeud3" {
  ami                    = var.ami_id
  instance_type          = var.vm_type
  key_name               = aws_key_pair.generated.key_name
  subnet_id              = aws_subnet.subnet.id
  vpc_security_group_ids = ["${aws_security_group.master.id}"]

  user_data = <<-EOL
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -yf python3 python3-pip
    sudo adduser ${var.user}
    echo "${var.user}:${var.password}" | sudo chpasswd
    sudo usermod -aG sudo ${var.user}
    sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
    sudo systemctl restart ssh
    EOL
  tags = {
    Name = "noeud3"
  }

  depends_on = [
    aws_vpc.vpc,
    aws_subnet.subnet,
  ]
}

resource "aws_instance" "master" {
  ami                         = var.ami_id
  instance_type               = var.vm_type
  key_name                    = aws_key_pair.generated.key_name
  subnet_id                   = aws_subnet.subnet.id
  associate_public_ip_address = true
  vpc_security_group_ids      = ["${aws_security_group.master.id}"]

  depends_on = [
    aws_vpc.vpc,
    aws_subnet.subnet,
    aws_security_group.master,
    aws_instance.noeud1,
    aws_instance.noeud2,
    aws_instance.noeud3
  ]

  user_data = <<-EOL
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -yf sshpass python3 python3-pip ansible

    cat << EOF > /home/admin/hosts
    [all]
    ${aws_instance.noeud1.private_ip}
    ${aws_instance.noeud2.private_ip}
    ${aws_instance.noeud3.private_ip}

    [all:vars]
    ansible_ssh_pass=${var.password}
    ansible_beome_pass=${var.password}
    EOF

    cat << EOF > /home/admin/ansible.cfg
    [defaults]
    enable_plugins = yaml, ini
    inventory = /home/admin/hosts
    interpreter_python=/usr/bin/python3
    host_key_checking=False
    EOF
    EOL

  tags = {
    Name = "master"
  }
}

output "master_ip" {
  value = aws_instance.master.private_ip
}

output "master_public_ip" {
  value = aws_instance.master.public_ip
}

output "noeud1_ip" {
  value = aws_instance.noeud1.private_ip
}

output "noeud2_ip" {
  value = aws_instance.noeud2.private_ip
}

output "noeud3_ip" {
  value = aws_instance.noeud3.private_ip
}

# export for Ansible hosts file