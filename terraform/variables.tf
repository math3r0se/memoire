variable "ami_id" {
  default = "ami-002ff2c881c910aa8"
}

variable "vm_type" {
  default = "t2.micro"
}

variable "vpc_cidr" {
  default = "10.0.0.0/24"
}

variable "user" {
  default = "admin"
}

variable "password" {
  default = "admin"
}

resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated" {
  key_name   = "generated-key"
  public_key = tls_private_key.key.public_key_openssh
}

output "private_key" {
  value     = tls_private_key.key.private_key_pem
  sensitive = true
}