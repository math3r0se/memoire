#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Fonction qui appelle la fonction qui génère la couleur */
void rand_color(int *r, int *v, int *b);

int main(void){
    int r, v, b;
    srand(time(NULL));

    /* Test de crash */

    rand_color(&r, &v, &b);

    if((r >= 0 && r <= 255) && (v >= 0 && v <= 255) && (b >= 0 && b <= 255)){
        printf("Couleur: #%02x%02x%02x\n", r, v, b);
    }
    else{
        printf("Erreur: couleur non générée\n");
        return 1;
    }

    return 0;
}

void rand_color(int *r, int *v, int *b){
    *r = rand() % 256;
    *v = rand() % 256;
    *b = rand() % 256;
}