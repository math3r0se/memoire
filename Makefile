MAJOR = 1
MINOR = 1
PATCH = 0
NAME = main

CC = gcc
CFLAGS = -Werror -Wextra -pedantic-errors
STD = gnu89

build:
	$(CC) $(CFLAGS) -std=$(STD) -o $(NAME) main.c